# Umpire Timer

This program provides a timer for a table tennis umpire.

Part of the project "Open-TT" which provides open documents and applications for table tennis:

- <https://gitlab.com/open-tt>

[changelog](changelog.md)

**Important:** the focus of development at the moment is *features*, not safety.
Backup your files regularly, please.

## Git-Repository

The branching model regards to the stable mainline model described in <http://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git>

This means, there is always a stable mainline, the `main` branch.
This branch ist always compileable and testable, both without errors.

## Legal stuff

### Licenses

License of the documents: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
See file [LICENSE](LICENSE).

License of the programs: GNU General Public License.
See file [COPYING](COPYING).

Which means:

- the documents are free, as long as you
	- don't make money with them
	- mention the creator
	- share derivates with the same license
- programs are free and open source
	- you can use the program as you want, even commercially
	- you can share the program as you like
	- if you change the source code, you have to distribute it under the same license, and you have to provide the source code of the programs


### Copyright

Copyright 2021-2021 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License.

See [COPYING](COPYING) for details.

This file is part of Open-TT: Umpire Timer.

Open-TT: Umpire Timer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Open-TT: Umpire Timer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Open-TT: Umpire Timer. If not, see <http://www.gnu.org/licenses/>.
